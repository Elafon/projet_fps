﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {

    public float MouseSensitivity;

    public Vector2 clampInDegrees = new Vector2(360, 180);

    private Vector2 targetDirection;
    private Vector2 targetCharacterDirection;
    Vector2 _mouseAbsolute;
    Vector2 _smoothMouse;

    Vector2 smoothing = new Vector2(3, 3);

    private Vector2 inputMouse;

    public GameObject myCharacter;

    private bool isActive;

	// Use this for initialization
	void Start ()
    {
        //myCharacter = GetComponentInParent<Transform>();

        // Set target direction to the camera's initial orientation.
        targetDirection = transform.localRotation.eulerAngles;
        targetCharacterDirection = myCharacter.transform.localRotation.eulerAngles;


        inputMouse = Vector2.zero;

        isActive = true;

        LockCursor(true);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Rotation de base
        Quaternion targetOrientation = Quaternion.Euler(targetDirection);
        Quaternion targetCharacterOrientation = Quaternion.Euler(targetCharacterDirection);

        if (isActive)
        {
            MouseMouvement(); //recuperation des input
        }

        Vector2 mouseDelta = Vector2.Scale(inputMouse, new Vector2(MouseSensitivity, MouseSensitivity));

        // Interpolate mouse movement over time to apply smoothing delta.
        _smoothMouse.x = Mathf.Lerp(_smoothMouse.x, mouseDelta.x, 1f / smoothing.x);
        _smoothMouse.y = Mathf.Lerp(_smoothMouse.y, mouseDelta.y, 1f / smoothing.y);

        _mouseAbsolute += _smoothMouse;

        Clamp();

        transform.localRotation = Quaternion.AngleAxis(-_mouseAbsolute.y, targetOrientation * Vector3.right) * targetOrientation;
        //Debug.Log("ici aussi + " + transform.localRotation);

        if (myCharacter)
        {
            Quaternion yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, Vector3.up);
            myCharacter.transform.localRotation = yRotation * targetCharacterOrientation;
        }
        else
        {
            Quaternion yRotation = Quaternion.AngleAxis(_mouseAbsolute.x, transform.InverseTransformDirection(Vector3.up));
            transform.localRotation *= yRotation;
        }
    }

    // Detection des mouvements de la souris
    private void MouseMouvement()
    {
        inputMouse = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        //Debug.Log("ici + " + inputMouse);
    }


    private void Clamp()
    {
        // Clamp and apply the local x value first, so as not to be affected by world transforms.
        if (clampInDegrees.x < 360)
            _mouseAbsolute.x = Mathf.Clamp(_mouseAbsolute.x, -clampInDegrees.x * 0.5f, clampInDegrees.x * 0.5f);

        // Then clamp and apply the global y value.
        if (clampInDegrees.y < 360)
            _mouseAbsolute.y = Mathf.Clamp(_mouseAbsolute.y, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);
    }


    public void LockCursor(bool _active)
    {
        if (_active)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }





    /*=============================
     *          Mutateur
     *             &
     *          Accesseur
     ==============================*/


    public void set_isActive(bool _active) //Mutateur de la variable isActive
    {
        isActive = _active;
    }

    public bool get_isActive() //retourne la variable isActive
    {
        return isActive;
    }
}
