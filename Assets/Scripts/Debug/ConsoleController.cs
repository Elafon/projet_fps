﻿/// <summary>
/// Handles parsing and execution of console commands, as well as collecting log output.
/// Copyright (c) 2014-2015 Eliot Lash
/// </summary>
using UnityEngine;

using System;
using System.Collections.Generic;
using System.Text;

public delegate void CommandHandler(string[] args);

public class ConsoleController
{

    #region Event declarations
    // Used to communicate with ConsoleView
    public delegate void LogChangedHandler(string[] log);
    public event LogChangedHandler logChanged;

    public delegate void VisibilityChangedHandler(bool visible);
    public event VisibilityChangedHandler visibilityChanged;
    #endregion


    class CommandRegistration
    {
        public string command { get; private set; }
        public CommandHandler handler { get; private set; }
        public string help { get; private set; }

        public CommandRegistration(string command, CommandHandler handler, string help) //Enregistreur de commande
        {
            this.command = command;
            this.handler = handler;
            this.help = help;
        }
    }


    const int scrollbackSize = 20;
    Queue<string> scrollback = new Queue<string>(scrollbackSize); //Commande en attente?
    List<string> commandHistory = new List<string>(); //Historique des commandes
    Dictionary<string, CommandRegistration> commands = new Dictionary<string, CommandRegistration>(); //Dico des commandes enregistré

    public string[] log { get; private set; } //Copy of scrollback as an array for easier use by ConsoleView

    const string repeatCmdName = "$$";

    public ConsoleController()
    {
        registerCommand("playerlife", playerlife, "change life player");
        registerCommand("toto", toto, "write toto in console");
        registerCommand("help", help, "write all commandes register");
    }

    void registerCommand(string _command, CommandHandler _handler, string _help) //fonction d'ajouts de commandes
    {
        commands.Add(_command, new CommandRegistration(_command, _handler, _help)); //Rajoute la commande dans le dictionnaire en créant une commande
    }


    public void appendLogLine(string line)
    {
        Debug.Log(line);

        if (scrollback.Count >= ConsoleController.scrollbackSize)
        {
            scrollback.Dequeue();
        }
        scrollback.Enqueue(line);

        log = scrollback.ToArray();
        if (logChanged != null)
        {
            logChanged(log);
        }
    }

    public void runCommandString(string commandString)
    {
        appendLogLine("$ " + commandString);

        string[] commandSplit = parseArguments(commandString);
        string[] args = new string[0];

        if (commandSplit.Length < 1)
        {
            appendLogLine(string.Format("Unable to process command '{0}'", commandString));
            return;

        }
        else if (commandSplit.Length >= 2)
        {
            int numArgs = commandSplit.Length - 1;
            args = new string[numArgs];
            Array.Copy(commandSplit, 1, args, 0, numArgs);
        }

        runCommand(commandSplit[0].ToLower(), args); //Lance la commande
        commandHistory.Add(commandString); //ajoute a l'historique des commandes
    }

    public void runCommand(string command, string[] args)
    {
        CommandRegistration reg = null;
        if (!commands.TryGetValue(command, out reg))
        {
            appendLogLine(string.Format("Unknown command '{0}', type 'help' for list.", command));
        }
        else
        {
            if (reg.handler == null)
            {
                appendLogLine(string.Format("Unable to process command '{0}', handler was null.", command));
            }
            else
            {
                reg.handler(args);
            }
        }
    }

    static string[] parseArguments(string commandString)
    {
        LinkedList<char> parmChars = new LinkedList<char>(commandString.ToCharArray());
        bool inQuote = false;
        var node = parmChars.First;
        while (node != null)
        {
            var next = node.Next;
            if (node.Value == '"')
            {
                inQuote = !inQuote;
                parmChars.Remove(node);
            }
            if (!inQuote && node.Value == ' ')
            {
                node.Value = '\n';
            }
            node = next;
        }
        char[] parmCharsArr = new char[parmChars.Count];
        parmChars.CopyTo(parmCharsArr, 0);
        return (new string(parmCharsArr)).Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
    }





    #region Command handlers

    void playerlife(string[] arg )
    {
        GameObject.Find("Player").GetComponent<PlayerInfo>().playerLife = int.Parse(arg[0]);
        appendLogLine("Player life was change to " + arg[0]);
    }

    void toto(string[] arg)
    {
        appendLogLine("toto");
    }

    void help(string[] arg)
    {
        foreach (CommandRegistration reg in commands.Values)
        {
            appendLogLine(string.Format("{0}: {1}", reg.command, reg.help));
        }
    }

    #endregion
}
