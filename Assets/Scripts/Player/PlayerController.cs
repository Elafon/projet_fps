﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //public Camera myCamera;

    //Vitesse
    public float speedWalk;
    public float speedRun;

    //Rigidbody du player
    private Rigidbody myRigidbody;

    private bool isRunning;
    private bool isForward;
    private bool isBackward;
    private bool isStarfLeft;
    private bool isStarfRight;
    private bool isJump;
    private bool isGrounded;
   // private bool inJump;

    void Awake()
    {

    }


	// Use this for initialization
	void Start ()
    {
        //myCamera = GetComponentInChildren<Camera>();

        //Recuperation du rigidbody
        myRigidbody = GetComponent<Rigidbody>();

        //Init variable bool
        isRunning = false;
        isForward = false;
        isBackward = false;
        isStarfLeft = false;
        isStarfRight = false;
        isJump = false;
        isGrounded = true;
        // inJump = false;

        GlobalInteract.get_Instance().InitMouseLook();
    }
	
	// Update is called once per frame
	void Update ()
    {
        CheckGrounded();
    }

    void FixedUpdate()
    {
        InputDetection();
        Movement();
    }

    //Detection des inputs player
    void InputDetection()
    {
        //Si le player va en avant

        if (Input.GetKey(InputManager.get_Instance().myInput.Forward))
        {
            isForward = true;
            isBackward = false;

            #if UNITY_EDITOR
            Debug.Log("Forward : " + isForward + " Backward : " + isBackward);
            #endif
        }
        else if (Input.GetKey(InputManager.get_Instance().myInput.Backward)) //Si le player va en arriere
        {
            isBackward = true;
            isForward = false;

            #if UNITY_EDITOR
            Debug.Log(" Backward : " + isBackward + " Forward : " + isForward);
            #endif
        }
        else
        {
            isForward = false;
            isBackward = false;
        }

        //Si le player va sur la droite
        if (Input.GetKey(InputManager.get_Instance().myInput.Straf_Right))
        {
            isStarfRight = true;
            isStarfLeft = false;

            #if UNITY_EDITOR
            Debug.Log("Starf Right : " + isStarfRight + " Straf Left : " + isStarfLeft);
            #endif
        }
        else if (Input.GetKey(InputManager.get_Instance().myInput.Straf_Left)) //Si le player va sur la gauche
        {
            isStarfLeft = true;
            isStarfRight = false;            

            #if UNITY_EDITOR
            Debug.Log("Starf Left : " + isStarfLeft + " Straf Right : " + isStarfRight);
            #endif
        }
        else
        {
            isStarfRight = false;
            isStarfLeft = false;
        }

        

        if (Input.GetKeyDown(InputManager.get_Instance().myInput.Jump) && isGrounded == true)
        {
            isJump = true;
            isGrounded = false;
            Debug.Log("saut");

            StartCoroutine(timeJump());
        }
    }

    void Movement()
    {
        //Mouvement voulus
        Vector3 DesiredMove = new Vector3(0.0f, 0.0f, 0.0f);

        if (isForward)
        {
            DesiredMove += transform.forward;
        }
        else if(isBackward)
        {
            DesiredMove -= transform.forward;
        }

        if (isStarfRight)
        {
            DesiredMove += transform.right;
        }
        else if (isStarfLeft)
        {
            DesiredMove -= transform.right;
        }        

        //Application du mouvement voulus
        if (DesiredMove != Vector3.zero)
        {
            myRigidbody.MovePosition(transform.position + (DesiredMove * speedWalk) * Time.deltaTime);
        }

        if (isJump)
        {
            myRigidbody.AddForce((transform.up * speedWalk), ForceMode.Impulse);

            isJump = false;
        }
    }


    void CheckGrounded()
    {
        RaycastHit hit;

        Debug.DrawRay(transform.position, -transform.up, Color.green);

        if (Physics.Raycast(transform.position, -transform.up, out hit, 1.2f))
        {
            Debug.Log("sa touche le sol");

            isGrounded = true;            
        }
        else
        {
            isGrounded = false;
        }

    }

    IEnumerator timeJump()
    {
        yield return new WaitForSeconds(1.2f);

        isJump = false;
    }
}
