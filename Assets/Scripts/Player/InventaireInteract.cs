﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventaireInteract : MonoBehaviour {

    public RectTransform inventaire;
    public Camera myCamera;

    public RectTransform itemUI;

    private bool isOpen;

    private List<ItemInfo> myInventory;

    private int limitItim;



	// Use this for initialization
	void Start () {
        isOpen = false;

        limitItim = 1;
        myInventory = new List<ItemInfo>();
    }
	
	// Update is called once per frame
	void Update () {
		
        //Input.GetKeyDown(KeyCode.Tab)

        if (Input.GetKeyDown(InputManager.get_Instance().myInput.Inventory))
        {
            isOpen = !isOpen;
            OpenClose(isOpen);
        }

        raycastItem();

    }

    void OpenClose(bool _action)
    {
        if (_action) //Si true, ouvre l'inventaire et declenche l'animation
        {
            inventaire.gameObject.SetActive(_action);
            inventaire.GetComponent<Animation>().Play();

            GlobalInteract.get_Instance().UnlockMouseCursor(); //unlock le curseur de la souris
        }
        else //Si false ferme l'inventaire
        {
            inventaire.gameObject.SetActive(_action);

            GlobalInteract.get_Instance().LockMouseCursor(); //lock le curseur de la souris
        }
    }

    void raycastItem()
    {
        RaycastHit hit;

        if (Physics.Raycast(myCamera.transform.position, myCamera.transform.forward,out hit, 5.0f))
        {
            if (hit.collider.tag == "item")
            {
                itemUI.gameObject.SetActive(true);
                itemUI.GetComponentInChildren<Text>().text = hit.collider.GetComponent<ItemInfo>().strName;

                GrabItem(hit.collider.GetComponent<ItemInfo>());
            }
            else
            {
                itemUI.gameObject.SetActive(false);
            }

        }
        else
        {
            itemUI.gameObject.SetActive(false);
        }
    }

    void GrabItem(ItemInfo _object)
    {
        if (Input.GetKeyDown( InputManager.get_Instance().myInput.Action))
        {
            if (_object.strName == "BackPack")
            {
                myInventory.Add(_object);

                Destroy(_object.gameObject);

                limitItim = 4;

                Debug.Log("je chope le sac");
                inventaire.GetChild(4).GetComponent<Image>().sprite = myInventory[myInventory.Count - 1].itemImage;

                for (int i = 0; i < 4; i++)
                {
                    inventaire.GetChild(i).gameObject.SetActive(true);
                }
            }
            else if (_object.strName != "BackPack" && limitItim > 1 && myInventory.Count-1 < limitItim)
            {
                myInventory.Add(_object);

                Destroy(_object.gameObject);

                Debug.Log("je chope autre chose  " + myInventory.Count);
                inventaire.GetChild(myInventory.Count-2).GetComponent<Image>().sprite = myInventory[myInventory.Count - 1].itemImage;

            }
        }
    }
}
