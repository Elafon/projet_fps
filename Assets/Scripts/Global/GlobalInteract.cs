﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInteract : MonoBehaviour {

    /* ========================
    *     Section singleton
    *      initialisation
    * ========================= */

    private static GlobalInteract myInstance; //Instance du singleton

    public static GlobalInteract get_Instance() //Accesseur singleton
    {
        if (myInstance != null) //Si attribué
        {
            return myInstance; //On retrourne le singleton
        }
        else //sinon
        {
            return myInstance = new GlobalInteract(); //création et retour du singleton
        }
    }

	// Use this for initialization
	void Start ()
    {
        myInstance = this; //Attribution du singleton

        Application.targetFrameRate = 60;

        DontDestroyOnLoad(this.gameObject); //On ne détruit pas le singleton
    }

    public void InitMouseLook()
    {
        myMouselook = GameObject.Find("Camera").GetComponent<MouseLook>();
        myShoot = GameObject.Find("Camera").GetComponentInChildren<RaycatsShoot>();
    }

    /*____________________________________________________________________________________________________________________________________________________________________________________________________
      ____________________________________________________________________________________________________________________________________________________________________________________________________*/


    /* ==================
     *     Section
     *   Mouse gestion
     * ================= */

    //Recuperation du component MouseLook sur la camera
    private MouseLook myMouselook;
    private RaycatsShoot myShoot;

    public void UnlockMouseCursor()
    {
        myMouselook.LockCursor(false); //unlock le curseur
        myMouselook.set_isActive(false); //Stop le mouselook
        myShoot.set_isActive(false); //stop le shoot
    }

    public void LockMouseCursor()
    {
        myMouselook.LockCursor(true); //lock le curseur
        myMouselook.set_isActive(true); //active le mouselook
        myShoot.set_isActive(true); //active le shoot
    }
}
