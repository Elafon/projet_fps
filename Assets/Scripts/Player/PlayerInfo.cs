﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {

    public int playerLife;
    /*{        
        get { return playerLife; }
        set { playerLife = value; }

    }*/

    public Text UILife;

	// Use this for initialization
	void Start () {

        playerLife = 100;

	}
	
	// Update is called once per frame
	void Update () {
        
    }

    void FixedUpdate()
    {
        Update_LifeUI();
    }

    void Update_LifeUI()
    {
        UILife.text = playerLife.ToString();
    }


    

}
