﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInteract : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadScene(int _scene)
    {
        SceneManager.LoadScene(_scene);

        //StartCoroutine(InitMouse());
    }

    IEnumerator InitMouse()
    {
        yield return new WaitForSeconds(20.0f);
        GlobalInteract.get_Instance().InitMouseLook();
    }
}
