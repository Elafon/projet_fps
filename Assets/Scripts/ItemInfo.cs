﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum itemType
{
    Munition_50mm,
    Munition_5mm,
    vie_10,
    vie_50,
}


public class ItemInfo : MonoBehaviour {

    public string strName;
    public itemType iType;

    public int nbrRestant;

    public Sprite itemImage;

	// Use this for initialization
	/*void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}*/
}
