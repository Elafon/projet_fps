﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour {

    public GameObject[] listPrefab;

    public int rngItem;

	// Use this for initialization
	void Start () {
        LaunchRandom();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void LaunchRandom()
    {
        int nbr = Random.Range(0, 99); //Random pour savoir si il y aura un item

        if (nbr <= rngItem)
        {
            int nbrItem = Random.Range(0, listPrefab.Length);

            Debug.Log(nbrItem);

            GameObject newItem = GameObject.Instantiate(listPrefab[nbrItem]); //Instance de l'item

            newItem.transform.SetParent(gameObject.transform); //Set du parent

            newItem.transform.position = new Vector3(gameObject.transform.position.x, newItem.transform.position.y, gameObject.transform.position.z); //set de la position
        }
        else //Si pas d'item
        {
            Destroy(gameObject);
        }
    }
}
