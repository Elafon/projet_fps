﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public struct inputConfig
    {
        public string Action;
        public string Forward;
        public string Backward;
        public string Straf_Left;
        public string Straf_Right;
        public string Jump;
        public string Inventory;

    }

    public inputConfig myInput;

    private static InputManager m_instance;

	// Use this for initialization
	void Start () {

        m_instance = this;

        myInput.Action = "e";
        myInput.Forward = "z";
        myInput.Backward = "s";
        myInput.Straf_Left = "q";
        myInput.Straf_Right = "d";
        myInput.Jump = "space";
        myInput.Inventory = "tab";

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    public static InputManager get_Instance()
    {
        if (m_instance != null)
        {
            return m_instance;
        }
        else
        {
            return m_instance = new InputManager();
        }
    }
}
